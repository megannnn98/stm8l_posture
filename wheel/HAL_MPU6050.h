
/* Define to prevent recursive inclusion*/
#ifndef __HAL_MPU6050_H
#define __HAL_MPU6050_H

#ifdef __cplusplus
 extern "C" {
#endif 
  
/* Includes */

#include  "stm8l15x_i2c.h"
#include <stdlib.h>
/**
* @addtogroup  MPU6050_I2C_Define
* @{
*/

#define MPU6050_I2C 				I2C1
#define MPU6050_CLK_Peripheral_I2C1 CLK_Peripheral_I2C1
   
#define MPU6050_SCL_GPIO            GPIOC
#define MPU6050_SDA_GPIO            GPIOC

#define MPU6050_SCL_PIN         	GPIO_Pin_1
#define MPU6050_SDA_PIN         	GPIO_Pin_0
#define MPU6050_Speed           	(100uL*1000uL) // 100kHz standard mode

/**
*@}
*/ /* end of group MPU6050_I2C_Define */ 


#endif /* __HAL___MPU6050_H */

/******************* (C) COPYRIGHT 2012 Harinadha Reddy Chintalapalli *****END OF FILE****/
