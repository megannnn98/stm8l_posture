/**
  ******************************************************************************
  * @file    calcCRC.h
  * @author  Karatanov M.N.
  * @version 0.1
  * @date    18-June-2015
  * @brief   crc functions header
  ******************************************************************************
  */

#pragma once

/* Private define ------------------------------------------------------------*/
#define CRC_OK 		((u8)0)
#define CRC_ERROR	(!(CRC_OK))


/* Private function prototypes -----------------------------------------------*/
unsigned short CRC16(unsigned char* p, unsigned char len);
bool checkPkt(u8* iY);


/**
  * @}
  */
  
/**
  * @}
  */

/************************END OF FILE****/