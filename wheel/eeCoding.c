

/*
* ������ �������� ������� ������ � ������ EEPROM
*/

// ** I_EE_NUMPULT

/* Includes ------------------------------------------------------------------*/
#include "utils.h"
#include "eeCoding.h"
#include "processCRC.h"

void MEM_readPkt(u16 startAddr, u8* ptr, const u16 len)
{
	for(u16 i = 0 ; i < len ; i++)
	{
		ptr[i] = MEM_readByte(startAddr + i);		
	}
}


void MEM_writePkt(u16 startAddr, u8* ptr, const u16 len)
{
	
    EEPROM_UNLOCK;
		
	for(u16 i = 0 ; i < len ; i++)
	{
		FLASH_ProgramByte(startAddr + i, ptr[i]);
		while(SET == FLASH_GetFlagStatus(FLASH_FLAG_EOP));
	}
	
    EEPROM_LOCK;
}




void MEM_write32withCRC(u16 adress16, void* dataPtr)
{
	u16 CRC_calc = 0;
	u8 t = 0;
	
	CRC_calc = CRC16(dataPtr, 4);
	
	MEM_writePkt(adress16, dataPtr, 4);
	
	t = BYTE_0(CRC_calc);	
	MEM_writePkt(adress16 + 4, &t, 1);
	
	t = BYTE_1(CRC_calc);	
	MEM_writePkt(adress16 + 5, &t, 1);
}


u8 MEM_read32withCRC(u16 adress16, void* dataPtr)
{
	u16 CRC_calc = 0, CRC_read = 0;
	
	MEM_readPkt(adress16, dataPtr, 4);
	CRC_read = ((u16)MEM_readByte(adress16+5) << 8) + ((u16)MEM_readByte(adress16+4));
	
	
	CRC_calc = CRC16(dataPtr, 4);

	
	if(CRC_calc == CRC_read)
	{
	  	return STATUS_OK;
	}
	return STATUS_ERROR;
}

// ** ������ ���� ����� 4 ������� ����������
u8 MEM_writeDataTwoCopyes(u16 adressInMem, void* pDataForWrite)
{
  	u32 iTmp1ForRead = 0UL, iTmp2ForRead = 0UL;
  
  	bool flag1ok = false;
  	bool flag2ok = false;

	
	// ** ������ ����� ���������� 
	if (STATUS_OK == MEM_read32withCRC(adressInMem, &iTmp1ForRead))
	{
		flag1ok = true;
	}
	if (STATUS_OK == MEM_read32withCRC(adressInMem + MEM_BLOCK_SIZE, &iTmp2ForRead))
	{
		flag2ok = true;
	}	
	
	// ** CRC ����� ���������
	if ((true == flag1ok) && (true == flag2ok))
	{
		// ������� ������ �� ������� ���� ���������� ������
		if ((iTmp1ForRead == iTmp2ForRead) && (iTmp1ForRead == *(u32*)pDataForWrite))
		{
		}
		// ����� ������
		else
		{
			// ** ������� - ������ ������, ����� � ������ 
			MEM_write32withCRC(adressInMem + MEM_BLOCK_SIZE, pDataForWrite);
			if (1 == MEM_read32withCRC(adressInMem + MEM_BLOCK_SIZE, &iTmp1ForRead))
			{
				MEM_write32withCRC(adressInMem, pDataForWrite);
			}
			else
			{
				return STATUS_ERROR;
			}
		}
	}
  
	// ** ������ ������
	else if ((false == flag1ok) && (true == flag2ok))
	{
		// ** ������� - ������ ������, ����� � ������ 
		MEM_write32withCRC(adressInMem, pDataForWrite);
		if (STATUS_OK == MEM_read32withCRC(adressInMem, &iTmp1ForRead))
		{
			MEM_write32withCRC(adressInMem + MEM_BLOCK_SIZE, pDataForWrite);
		}
		else
		{
			return STATUS_ERROR;
		}
	}

	// ** ������ ������
	else if ((true == flag1ok) && (false == flag2ok))
	{
		// ** ������� - ������ ������, ����� � ������ 
		MEM_write32withCRC(adressInMem + MEM_BLOCK_SIZE, pDataForWrite);
		if (STATUS_OK == MEM_read32withCRC(adressInMem + MEM_BLOCK_SIZE, &iTmp1ForRead))
		{
			MEM_write32withCRC(adressInMem, pDataForWrite);
		}
		else
		{
			return STATUS_ERROR;
		}
	}	
	// ** ��� �����
	else
	{
		// ** ����� ���� ��� �����
		MEM_write32withCRC(adressInMem, pDataForWrite);
		MEM_write32withCRC(adressInMem + MEM_BLOCK_SIZE, pDataForWrite);
		
		return STATUS_ERROR;
	}
		
	return STATUS_OK;
}


// ** ������ ����� ���������� ����� 4� ������� ����������
u8 MEM_readDataTwoCopyes(u16 adressInMem, void* dataPtr, u32 defaultVal)
{
	u32   data_r[2]    = {0ul};
	bool iFlagERROR[2] = {false, false};
	
	// ** ������ ������ � ��������� �� CRC 
	// ** ���� ������ - ���������� �����
	
	if (STATUS_ERROR == MEM_read32withCRC(adressInMem, &data_r[0]))
	{
		iFlagERROR[0] = true;
	}
	if (STATUS_ERROR == MEM_read32withCRC(adressInMem + MEM_BLOCK_SIZE, &data_r[1]))
	{
		iFlagERROR[1] = true;
	}
	
	// ** ��� ����� ���������� 
	if ((false == iFlagERROR[0]) && (false == iFlagERROR[1]))
	{
		*(u32*)dataPtr = data_r[0];
		
		return STATUS_OK;
	}
	// ** ������ ����� ��������, ��������� ������ ���������� � ��� �����
	else if ((false == iFlagERROR[0]) && (true == iFlagERROR[1]))
	{
		MEM_writeDataTwoCopyes(adressInMem, &data_r[0]);
		*(u32*)dataPtr = data_r[0];
		
		return STATUS_WAS_ERROR;
	}
	// ** ������ ����� ��������, ��������� ������ ���������� � ��� �����
	else if ((true == iFlagERROR[0]) && (false == iFlagERROR[1]))
	{
		MEM_writeDataTwoCopyes(adressInMem, &data_r[1]);
		*(u32*)dataPtr = data_r[1];
		
		return STATUS_WAS_ERROR;
	}	
	// ** ��� �������
	MEM_writeDataTwoCopyes(adressInMem, &defaultVal);
	*(u32*)dataPtr = defaultVal;

	return STATUS_ERROR;	
}
