/* Includes ------------------------------------------------------------------*/
#include "utils.h"
#include "MPU6050.h" 
#include "delay.h"
#include "eeCoding.h"
/* Private functions ---------------------------------------------------------*/

void getAccel(s8* ix, s8* iy, s8* iz);
uint16_t GetVcc(void);

s8 x, y, z;
s8 x_offset, y_offset, z_offset;


u8 middle_f   = 0;
s8 middle_cnt = 0;
s8 middle_x[3] = {0};
s8 middle_y[3] = {0};
s8 middle_z[3] = {0};

extern u8 Y[34];
extern u8 Z[34];
enum 
{
    NOT_READY = 0,
    READY
} motor_ready = NOT_READY;
u16 tmpVCC;

PT_THREAD (thread_radioSend(pt_t *pt));
pt_t radioSend_pt;

INTERRUPT_HANDLER(EXTI7_IRQHandler,15)
{
	while(1);
}

s8 middle_of_3(s8 a, s8 b, s8 c)
{
   s8 middle;

   if ((a <= b) && (a <= c)){
      middle = (b <= c) ? b : c;
   }
   else{
      if ((b <= a) && (b <= c)){
         middle = (a <= c) ? a : c;
      }
      else{
         middle = (a <= b) ? a : b;
      }
   }

   return middle;
}


PT_THREAD (thread_mpu(pt_t *pt))
{
    PT_BEGIN(pt);
    
    static u8 er = 0;
    static u8 data_ready = 0;
    x = y = z = 0;

    
    ATOMIC_START();
        motor_ready = NOT_READY;
        MPU6050_PWR_GPIO->ODR &= ~MPU6050_PWR_PIN;
		ATOMIC_STOP();
    
    
	  PT_DELAY_MS(MPU_TIMER, 100);  
    
    
		ATOMIC_START();
        MPU6050_PWR_GPIO->ODR |= MPU6050_PWR_PIN; 
    ATOMIC_STOP();
    
	  PT_DELAY_MS(MPU_TIMER, 100);
		
    
    ATOMIC_START();
        I2C_SoftwareResetCmd(I2C1, ENABLE);
        I2C_SoftwareResetCmd(I2C1, DISABLE);
        mpu6050.init_i2c();
        
        if(FALSE == mpu6050.testConnection(&er)) 
        {
            PT_RESTART(pt);
        }
        mpu6050.softReset(&er); if (er) {PT_RESTART(pt);}
		ATOMIC_STOP();
    
    
	  PT_DELAY_MS(MPU_TIMER, 100);    


// ���������� ������. �������, ������������ �� 8 MHz �����������    
// ���������� ��� ���������
// �������� ����, ������� ���� ��� ����� ������    
// Check that the MPU has been awoken
    
		ATOMIC_START();
        mpu6050.writeByte(MPU6050_RA_INT_ENABLE, MPU6050_INTERRUPT_DATA_RDY_BIT, &er); if (er) {msg.set(MPU_THREAD_RESET_MSG, 0, NULL);}
        mpu6050.writeByte(MPU6050_RA_PWR_MGMT_1, 0x00, &er);                           if (er) {msg.set(MPU_THREAD_RESET_MSG, 0, NULL);} 
        mpu6050.writeByte(MPU6050_RA_PWR_MGMT_2, 0x07, &er);                           if (er) {msg.set(MPU_THREAD_RESET_MSG, 0, NULL);}
        mpu6050.writeByte(MPU6050_RA_INT_PIN_CFG, (1 << 7)|(1 << 4), &er);             if (er) {msg.set(MPU_THREAD_RESET_MSG, 0, NULL);}
        mpu6050.writeByte(MPU6050_RA_CONFIG, MPU6050_PWR1_TEMP_DIS_BIT, &er);          if (er) {msg.set(MPU_THREAD_RESET_MSG, 0, NULL);}
        data_ready = mpu6050.readByte(MPU6050_RA_PWR_MGMT_1, &er);	                   if (er) {msg.set(MPU_THREAD_RESET_MSG, 0, NULL);}
		ATOMIC_STOP();
    
    	  PT_DELAY_MS(MPU_TIMER, 10);    
        PT_WAIT_WHILE(pt, MPU6050_ISR_PIN != MPU6050_ISR_GPIO->IDR & MPU6050_ISR_PIN);

    ATOMIC_START();
        data_ready = mpu6050.readByte(MPU6050_RA_INT_STATUS, &er);	                   if (er) {msg.set(MPU_THREAD_RESET_MSG, 0, NULL);}
        if(MPU6050_INTERRUPT_DATA_RDY_BIT != data_ready)
        {
           PT_RESTART(pt);
        }        
		ATOMIC_STOP();
    
    motor_ready = READY;
    while(1)
    { 

        if (MPU6050_ISR_PIN == (MPU6050_ISR_GPIO->IDR & MPU6050_ISR_PIN))
        {
            ATOMIC_START();
                data_ready = mpu6050.readByte(MPU6050_RA_INT_STATUS, &er); if (er) {PT_RESTART(pt);}
                if (0x01 == (data_ready & 0x01))
                {
                    getAccel(&x,&y,&z);
                    PT_DELAY_MS(MPU_TIMER, 100);
                }
            ATOMIC_STOP();
        }
        
        if (msg.getVal(MPU_NEW_OFFSET_MSG))
        {
            x_offset = x;
            y_offset = y;
            z_offset = z;
            
            MEM_writeDataTwoCopyes(I_EE_ACCEL_X, &x_offset);
            MEM_writeDataTwoCopyes(I_EE_ACCEL_Y, &y_offset);
            MEM_writeDataTwoCopyes(I_EE_ACCEL_Z, &z_offset);
        }
        
    }
    
    PT_END(pt);
}