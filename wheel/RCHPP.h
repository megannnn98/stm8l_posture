
#pragma once


/**
// ** ������ �������� ���������, ������� � ��������� �� RCHPP
\file
*/

#include "stm8l15x.h"


#define RD_BIT		 7
#define BURST_BIT	 6

/* Status registers */

#define PARTNUM          (0x30 | (1<<RD_BIT) | (1<<BURST_BIT))   /* Part number */
#define VERSION          (0x31 | (1<<RD_BIT) | (1<<BURST_BIT))   /* Current version number */
#define FREQEST          (0x32 | (1<<RD_BIT) | (1<<BURST_BIT))   /* Frequency offset estimate */
#define LQI              (0x33 | (1<<RD_BIT) | (1<<BURST_BIT))   /* Demodulator estimate for link quality */
#define RSSI             (0x34 | (1<<RD_BIT) | (1<<BURST_BIT))   /* Received signal strength indication */
#define MARCSTATE        (0x35 | (1<<RD_BIT) | (1<<BURST_BIT))   /* Control state machine state */
#define WORTIME1         (0x36 | (1<<RD_BIT) | (1<<BURST_BIT))   /* High byte of WOR timer */
#define WORTIME0         (0x37 | (1<<RD_BIT) | (1<<BURST_BIT))   /* Low byte of WOR timer */
#define PKTSTATUS        (0x38 | (1<<RD_BIT) | (1<<BURST_BIT))   /* Current GDOx status and packet status */
#define VCO_VC_DAC       (0x39 | (1<<RD_BIT) | (1<<BURST_BIT))   /* Current setting from PLL cal module */
#define TXBYTES          (0x3A | (1<<RD_BIT) | (1<<BURST_BIT))   /* Underflow and # of bytes in TXFIFO */
#define RXBYTES          (0x3B | (1<<RD_BIT) | (1<<BURST_BIT))   /* Overflow and # of bytes in RXFIFO */
#define RCCTRL1_STATUS   (0x3C | (1<<RD_BIT) | (1<<BURST_BIT))   /* Last RC oscilator calibration results */
#define RCCTRL0_STATUS   (0x3D | (1<<RD_BIT) | (1<<BURST_BIT))   /* Last RC oscilator calibration results */

/* Multi byte memory locations */

#define PATABLE          0x3E
#define TXFIFO           0x3F
#define RXFIFO           0x3F

/* Definitions for burst/single access to registers */

#define WRITE_BURST      0x40
#define READ_SINGLE      0x80
#define READ_BURST       0xC0

/* Strobe commands */

#define SRES             0x30        /* Reset chip. */
#define SFSTXON          0x31        /* Enable and calibrate frequency synthesizer (if MCSM0.FS_AUTOCAL=1). */
#define SXOFF            0x32        /* Turn off crystal oscillator. */
#define SCAL             0x33        /* Calibrate frequency synthesizer and turn it off */
#define SRX              0x34        /* Enable RX. Perform calibration first if switching from IDLE and MCSM0.FS_AUTOCAL=1. */
#define STX              0x35        /* Enable TX. Perform calibration first if IDLE and MCSM0.FS_AUTOCAL=1.  */
                                            /* If switching from RX state and CCA is enabled then go directly to TX if channel is clear. */
#define SIDLE            0x36        /* Exit RX / TX, turn off frequency synthesizer and exit Wake-On-Radio mode if applicable. */

#define SWOR             0x38        /* Start automatic RX polling sequence (Wake-on-Radio) */
#define SPWD             0x39        /* Enter power down mode when CSn goes high. */
#define SFRX             0x3A        /* Flush the RX FIFO buffer. */
#define SFTX             0x3B        /* Flush the TX FIFO buffer. */
#define SWORRST          0x3C        /* Reset real time clock. */
#define SNOP             0x3D        /* No operation. */

/* Modem Control */

#define MCSM0_XOSC_FORCE_ON  0x01


/* 
 * Chip Status Byte 
 */

/* Bit fields in the chip status byte */

#define STATUS_CHIP_RDYn_BM              0x80
#define STATUS_STATE_BM                  0x70
#define STATUS_FIFO_BYTES_AVAILABLE_BM   0x0F

/* Chip states */

#define STATE_MASK                       0x70
#define STATE_IDLE                       0x00
#define STATE_RX                         0x10
#define STATE_TX                         0x20
#define STATE_FSTXON                     0x30
#define STATE_CALIBRATE                  0x40
#define STATE_SETTLING                   0x50
#define STATE_RX_OVERFLOW                0x60
#define STATE_TX_UNDERFLOW               0x70

/* Values of the MACRSTATE register */

#define MARCSTATE_SLEEP                  0x00
#define MARCSTATE_IDLE                   0x01
#define MARCSTATE_XOFF                   0x02
#define MARCSTATE_VCOON_MC               0x03
#define MARCSTATE_REGON_MC               0x04
#define MARCSTATE_MANCAL                 0x05
#define MARCSTATE_VCOON                  0x06
#define MARCSTATE_REGON                  0x07
#define MARCSTATE_STARTCAL               0x08
#define MARCSTATE_BWBOOST                0x09
#define MARCSTATE_FS_LOCK                0x0A
#define MARCSTATE_IFADCON                0x0B
#define MARCSTATE_ENDCAL                 0x0C
#define MARCSTATE_RX                     0x0D
#define MARCSTATE_RX_END                 0x0E
#define MARCSTATE_RX_RST                 0x0F
#define MARCSTATE_TXRX_SWITCH            0x10
#define MARCSTATE_RXFIFO_OVERFLOW        0x11
#define MARCSTATE_FSTXON                 0x12
#define MARCSTATE_TX                     0x13
#define MARCSTATE_TX_END                 0x14
#define MARCSTATE_RXTX_SWITCH            0x15
#define MARCSTATE_TXFIFO_UNDERFLOW       0x16

/* Part number and version */

#define PARTNUM_VALUE                    0x00
#define VERSION_VALUE                    0x04

/*
 *  Others ... 
 */

#define LQI_CRC_OK_BM                    0x80
#define LQI_EST_BM                       0x7F

/*!
    \brief Class  cc1101

    \details Class contain all external functions for cc1101 
*/

struct Cc_driver
{
	u8 (*read)(u8 byte);
	void (*write)(u8 addr, u8 byte);
	u8 (*strobe)(u8 addr);

	void (*writeBrust_FIFO)(u8* ptr, u8 len);
	void (*readBrust_FIFO)(u8* ptr, u8 len);

	void (*reset)();
	void (*init)();
	void (*configGdoPins)();

	void (*setRXstate)();
	void (*setTXstate)();
	void (*setIDLEstate)();
	void (*setIDLE_SLEEPstate)();

	void (*setPower)(const u8 pwr);
	void (*setChannelNum)(const u8 num);
	
};

extern const struct Cc_driver cc;
