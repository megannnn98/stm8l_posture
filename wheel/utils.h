
#pragma once

#include  "stm8l15x.h"
#include <stdbool.h>
#include "pt.h"
#include "pt-sem.h"
#include "HAL.h"




#define DEBUG

#ifdef DEBUG
#else
	#define RELEASE
#endif



#define CLK_16MHz            CLK->CKDIVR = 0x00
#define CLK_8MHz             CLK->CKDIVR = 0x01
#define CLK_4MHz             CLK->CKDIVR = 0x02
#define CLK_2MHz             CLK->CKDIVR = 0x03
#define CLK_1MHz             CLK->CKDIVR = 0x04
#define CLK_05MHz            CLK->CKDIVR = 0x05
#define CLK_025MHz           CLK->CKDIVR = 0x06
#define CLK_0125MHz          CLK->CKDIVR = 0x07

#define NULL                    0L       
#define SWAP_NIBBLES(data)      ((data>>4)|(data<<4))


#define IWDG_RESET IWDG->KR=IWDG_KEY_REFRESH

void IWDG_Init(IWDG_Prescaler_TypeDef IWDG_Prescaler,u8 IWDG_Reload);



void spi_init();
void toHalt(u16 time);



enum
{
	MPU_THREAD_RESET_MSG = 0,//0
	MPU_NEW_OFFSET_MSG,
	BTN_PUSH_MSG,
	MOTOR_MSG,
  
	MAX_MSG,//3
};


typedef enum
{
	GENERAL_TIMER = 0,//0
	SEC_TIMER,//1
	MPU_TIMER,//2
	RADIO_TIMER,//3
	ASLEEP_TIMER,//4
	LED_BLINK_TIMER,//5
  BTN_TIMER,
  MOTOR_TIMER,
  
	MAX_TIMERS,
} Timer_t;


#define IS_BUTTON_PRESSED (BTN_PIN != (BTN_GPIO->IDR & BTN_PIN))

#define PT_DELAY_MS(timer, ms)	do { tim.reset(timer); \
								 PT_WAIT_WHILE(pt, tim.get(timer) < ms); } while(0)

#define PT_STATE_INIT() 	 static pt_t val_pt = {.lc = 0}; \
							 static pt_t *pt = &val_pt;

     
#define ATOMIC_START() do { disableInterrupts()
#define ATOMIC_STOP()  enableInterrupts(); } while(0)


enum {
    PR_MAX = 0,
    PR_HIGH,
    PR_MEDIUM,
    PR_LOW,
    PR_LOWEST,
};

// ������������ ������������ � ������������
//#define PT_INIT(pt, priority)   LC_INIT((pt)->lc ; (pt)->pri = priority)
//PT_PRIORITY_INIT
#define PT_RATE_INIT() char pt_pri_count = 0;
// maitain proority frame count
//PT_PRIORITY_LOOP ������������ ������� ���������
#define PT_RATE_LOOP() do { pt_pri_count = (pt_pri_count+1) & 0xf; } while(0);
// ��������� ����� � �����������
//PT_PRIORITY_SCHEDULE
// 5 �������
// �������� 0 ���� ������ -- ���������� ������ ����
// ��������� 1 -- ������  2 ����� 
// ��������� 2 -- ������  4 ������
// ��������� 3 -- ������  8 ������
// ��������� 4 -- ������ 16 ������
#define PT_PR_EXECUTE(rate, f) \
   do { if((PR_MAX     == rate) | \
           (PR_HIGH    == rate && (0 == (pt_pri_count & 0x01))) | \
           (PR_MEDIUM  == rate && (0 == (pt_pri_count & 0x03))) | \
           (PR_LOW     == rate && (0 == (pt_pri_count & 0x07))) | \
           (PR_LOWEST  == rate && (0 == (pt_pri_count & 0x0F)))) \
           {f;}; } while(0);


   
#define DELTA 20
#define ON_CONDITION (((x - x_offset) < -DELTA) || ((x - x_offset) > DELTA) || \
                      ((y - y_offset) < -DELTA) || ((y - y_offset) > DELTA))

#define OFF_CONDITION (!ON_CONDITION )
   
/* Global variables ---------------------------------------------------------*/

/** @defgroup Timer_driver
  * @{
  */
struct Timer_driver
{
	u32 (*get)(const Timer_t timer);
	u32 (*set)(const Timer_t timer, const u32 time);
	void (*reset)(const Timer_t timer);
	void (*init)();
};

/**
  * @}
  */
extern const struct Timer_driver tim;




/**
  * @{ @brief  ��������� ������� MSG.
*/
struct Msg_driver
{
	void (*init)();
    void (*process)(); 
    u8   (*set)(const u8 imsg, void* iparamPtr, u16 iparamSize);
    u8   (*repeat)(const u8 imsg);
    
	bool (*getVal)(const u8 imsg)	 ;
	void* (*getPtr)(const u8 imsg) ;
	u16   (*getSize)(const u8 imsg);
};
/**
  * @}
  */
extern const struct Msg_driver msg;













