

#include "utils.h"
#include "MPU6050.h" 
#include "calcCRC.h"
#include <stdlib.h>

extern u8 Y[34];
extern u8 Z[34];

u8* const pZfunc = Z+1;
extern s8 x_offset, y_offset, z_offset;
extern s16 x, y, z;
extern u16 tmpVCC;


void getAccel(s8* ix, s8* iy, s8* iz)
{
	u8 lower, upper;
	s16 raw;
  u8 e_code;
  
	lower = mpu6050.readByte(MPU6050_RA_ACCEL_XOUT_L, &e_code);  if (e_code) {  msg.set(MPU_THREAD_RESET_MSG, 0, NULL);  }
	upper = mpu6050.readByte(MPU6050_RA_ACCEL_XOUT_H, &e_code);  if (e_code) {  msg.set(MPU_THREAD_RESET_MSG, 0, NULL);  }
	raw = (((s16)upper << 8) | ((s16)lower << 0));
	*ix = (s8)(100.*((float)raw)/16384.);
  
	lower = mpu6050.readByte(MPU6050_RA_ACCEL_YOUT_L, &e_code);  if (e_code) {  msg.set(MPU_THREAD_RESET_MSG, 0, NULL);  }
	upper = mpu6050.readByte(MPU6050_RA_ACCEL_YOUT_H, &e_code);  if (e_code) {  msg.set(MPU_THREAD_RESET_MSG, 0, NULL);  }
	raw = (((s16)upper << 8) | ((s16)lower << 0));
	*iy = (s8)(100.*((float)raw)/16384.);
	
	lower = mpu6050.readByte(MPU6050_RA_ACCEL_ZOUT_L, &e_code);  if (e_code) {  msg.set(MPU_THREAD_RESET_MSG, 0, NULL);  }
	upper = mpu6050.readByte(MPU6050_RA_ACCEL_ZOUT_H, &e_code);  if (e_code) {  msg.set(MPU_THREAD_RESET_MSG, 0, NULL);  }
	raw = (((s16)upper << 8) | ((s16)lower << 0));
	*iz = (s8)(100.*((float)raw)/16384.);	
}

void getGyro(s16* ix_raw, s16* iy_raw, s16* iz_raw)
{
	u8 lower, upper;
  u8 e_code;
	lower = mpu6050.readByte(MPU6050_RA_GYRO_XOUT_L, &e_code);
	upper = mpu6050.readByte(MPU6050_RA_GYRO_XOUT_H, &e_code);
	*ix_raw = (((s16)upper << 8) | ((s16)lower << 0)) - x_offset;
//	*ix = ((float)raw)/16.4;	
	
	lower = mpu6050.readByte(MPU6050_RA_GYRO_YOUT_L, &e_code);
	upper = mpu6050.readByte(MPU6050_RA_GYRO_YOUT_H, &e_code);
	*iy_raw = (((s16)upper << 8) | ((s16)lower << 0)) - z_offset;
//	*iy = ((float)raw)/16.4;	
	
	lower = mpu6050.readByte(MPU6050_RA_GYRO_ZOUT_L, &e_code);
	upper = mpu6050.readByte(MPU6050_RA_GYRO_ZOUT_H, &e_code);
	*iz_raw = (((s16)upper << 8) | ((s16)lower << 0)) - z_offset;
//	*iz = ((float)raw)/16.4;	
}




/* Global variables ---------------------------------------------------------*/
volatile static struct {
    u8 val;
    u8* paramPtr;
    u16  paramSize;
} messages[MAX_MSG];

void set_unused_pins_output()
{
	GPIOA->DDR |= GPIO_Pin_2 | GPIO_Pin_3;
	GPIOB->DDR |= GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_6;
	GPIOD->DDR |= GPIO_Pin_0;
}


/**
  * @brief  ������������� � ������ ������� 
  * @param  ������������ �������
  * @param  �������� �� �������� ������� ����� �������������
  * @retval None
  */
void IWDG_Init(IWDG_Prescaler_TypeDef IWDG_Prescaler,u8 IWDG_Reload) 
{ 
	IWDG->KR  = IWDG_KEY_ENABLE;
	IWDG->KR  = IWDG_WriteAccess_Enable;
	
	IWDG->PR  = IWDG_Prescaler;
	IWDG->RLR = IWDG_Reload;
	
	IWDG->KR  = IWDG_KEY_REFRESH;
}

__near __no_init const unsigned char Factory_VREFINT @ 0x4910;


// ** ������� ������, ����� ������
void mcu_reset()
{
  	disableInterrupts();
  	IWDG_Init(IWDG_Prescaler_4, 0x01);			
	while(1);
}

// ** BTN_STATE **********************************


// ** ������� ������������ ������, � ������� �� ��������

void scanBtn()
{
  static enum 
  {
    BTN_RELEASE = 0,
    BTN_MAY_PUSH,
    BTN_PUSH,
    BTN_MAY_RELEASE,
  } pushState = BTN_RELEASE;
  
	switch(pushState)
	{
		case BTN_RELEASE:
		{
		  	if (IS_BUTTON_PRESSED) {pushState = BTN_MAY_PUSH;}
			else {pushState = BTN_RELEASE;}
		}
		break;
		case BTN_MAY_PUSH:
		{
		  	if (IS_BUTTON_PRESSED) {pushState = BTN_PUSH; 
                                msg.set(MPU_NEW_OFFSET_MSG, NULL, 0);}
			else {pushState = BTN_RELEASE;}
		}
		break;
		case BTN_PUSH:
		{
		  	if (IS_BUTTON_PRESSED) {pushState = BTN_PUSH;}
			else {pushState = BTN_MAY_RELEASE;}
		}
		break;
		case BTN_MAY_RELEASE:
		{
		  	if (IS_BUTTON_PRESSED) {pushState = BTN_PUSH;}
			else {pushState = BTN_RELEASE;}
		}
		break;
	}

}// end scan

PT_THREAD (thread_btn(pt_t *pt))
{
    static __istate_t s;
    s = __get_interrupt_state();  
    __enable_interrupt();
    PT_BEGIN(pt);
    
    tim.reset(BTN_TIMER);
    while(1)
    {
        scanBtn();
        
        tim.reset(BTN_TIMER);
				do 
        {
            __set_interrupt_state(s);
            PT_YIELD(pt);
        } while(tim.get(BTN_TIMER) < 30);        
        
    }
    PT_END(pt);
}






