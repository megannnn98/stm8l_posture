



#define MPU6050_PWR_PIN  GPIO_Pin_4
#define MPU6050_PWR_GPIO GPIOC

#define MPU6050_ISR_VAL  7
#define MPU6050_ISR_PIN  GPIO_Pin_7
#define MPU6050_ISR_GPIO GPIOB


#define BTN_PIN  GPIO_Pin_5
#define BTN_GPIO GPIOC


#define BLUE_PIN  GPIO_Pin_6
#define BLUE_GPIO GPIOC
#define GREEN_PIN  GPIO_Pin_5
#define GREEN_GPIO GPIOB

#define MOTOR_PIN  GPIO_Pin_0
#define MOTOR_GPIO  GPIOB

#define EXTI_SET_MPU_INT()  EXTI_SetPinSensitivity(EXTI_Pin_7, EXTI_Trigger_Falling);
#define EXTI_MPU_PIN	   	EXTI_Pin_7



#define RCHPP_SPI		SPI1
#define WAIT_UNTIL_MISO_BECOMES_LOW   while(SPI_MISO_PIN == (SPI_MISO_GPIO->IDR & SPI_MISO_PIN))

#define CS_HIGH                     do {SPI_CS_GPIO->ODR |=  SPI_CS_PIN; } while(0)
#define CS_LOW                      do {SPI_CS_GPIO->ODR &= ~SPI_CS_PIN; } while(0)

#define EXTI_SET_GDO()           	EXTI_SetPinSensitivity(EXTI_Pin_3, EXTI_Trigger_Falling)
#define EXTI_GDO0_PIN				EXTI_IT_Pin3


// GDO2 == 0 �������� ���������
// GDO2 == 1 ���������� ���������
#define IS_PREAMBLE_DETECTED 		(GDO2_PIN  == (GDO2_GPIO->IDR & GDO2_PIN))
#define IS_SYNC_DETECTED 			IS_GDO0_ACTIVE


/*
#define MPU6050_I2C_GPIO            GPIOC
#define MPU6050_I2C_SCL         	GPIO_Pin_1
#define MPU6050_I2C_SDA         	GPIO_Pin_0
*/