


/**
/brief ������ �������� ������� ��� ���������� ����������� cc1101 
*/

#include "RCHPP.h"
#include "HAL.h"
#include "delay.h"




// **********************************************************************************************************

//0x00 IOCFG	
	#define TEMP_SENSR_EN	7
	#define GDO_INV			6
	#define GDO_CFG			0
	
//0x03 FIFOTHR
	#define FIFO_THR		0
	#define CLOSE_IN_RX		4
	#define ADC_RETENTION	6
	
//0x07 PKTCTRL1
	#define PQT				5 //100
	#define CRC_AUTOFLUSH	3 //0
	#define APPEND_STATUS	2 //1
	#define ADR_CHK			0 //00
	
//0x08 PKTCTRL0
	#define WHITE_DATA		6
	#define PKT_FORMAT		4
	#define CRC_ENABLE		2
	#define LENGTH_CONFIG	0
	
//0x10 MDMCFG4
	#define CHANBW_E		6
	#define CHANBW_M		4
	#define DRATE_E			0

//0x11 MDMCFG3
	#define DRATE_M			0

//0x12 MDMCFG2				  // 0x1B
	#define DEM_DCFILT_OFF	        7 // 0
	#define MOD_FORMAT				4 // 1 GFSK
            #define FSK_2			0
            #define GFSK			1
            #define ASK_OOK			3
            #define FSK_4			4
            #define MSK			    7

	#define MANCHESTER_EN	        3 // 1
	#define SYNC_MODE		0 // 3 30/32 sync word detected

	
//0x13
	#define FEC_EN			7
	#define NUM_PREAMBLE	4
	#define CHANSPC_E		0
	
//0x14
	#define CHANSPC_M		0
	
//0x15 DEVIATN
	#define DEVIATION_E		4
	#define DEVIATION_M		0	
	
//0x16 MCSM2	
	#define RX_TIME_RSSI	4
	#define RX_TIME_QUAL	3
	#define RX_TIME			0

//0x17 MCSM1
	#define CCA_MODE		4
	#define RXOFF_MODE		2
	#define TXOFF_MODE		0
	
//0x18 MCSM0
	#define FS_AUTOCAL		4
	#define PO_TIMEOUT		2
	#define PIN_CTRL_EN		1
	#define XOSC_FORCE_ON	0
	
//0x19 FOCCFG
	#define FOC_BS_CS_GATE	5
	#define FOC_PRE_K		3
	#define FOC_POST_K		2
	#define FOC_LIMIT		0

//0x23 FSCAL3
	#define FSCAL_6			6	
	#define CHP_CURR_CAL_EN 4
	#define FSCAL_0			0


      

// **********************************************************************************************************

#define	CFG1101_00		    (0<<GDO_INV)|0x08	//IOCFG2        GDO2
#define	CFG1101_01		    0x2E				//IOCFG1        GDO1
#define	CFG1101_02		    (0<<GDO_INV)|0x06	//IOCFG0        GDO0

                                                //FIFOTHR (x)* TXFIFO-5, RXFIFO-60
#define	CFG1101_03		    (1<<ADC_RETENTION)|(0<<CLOSE_IN_RX)|(14<<FIFO_THR) // 0x4E

#define	CFG1101_04		    0xD1		// SYNC1 (x)*
#define	CFG1101_05		    0x93		// SYNC0 (x)*

#define	CFG1101_06		    32  		// PKTLEN (x)* 32 bytes
                                        // PKTCTRL1 (x)* NO ADDR, APPEND STATUS, PQT=4
#define	CFG1101_07		    (4<<PQT)|(0<<CRC_AUTOFLUSH)|(1<<APPEND_STATUS)|(0<<ADR_CHK) // 0x84

                                        // PKTCTRL0 (x)* FIX.PACKET, NO CRC
#define	CFG1101_08		    (0<<WHITE_DATA)|(0<<PKT_FORMAT)|(0<<CRC_ENABLE)|(0<<LENGTH_CONFIG) // 0x00
                                                 
#define	CFG1101_09		    0x00		//Device ADDRES 
#define	CFG1101_0A		    0x00		//CHANEL NUMBER (x)

#define	CFG1101_0B		    0x06		// FSCTRL1 (x)
#define	CFG1101_0C		    0x00		// FSCTRL0 (x)

#define	CFG1101_0D		    0x10		// FREQ2 (x) 433.099792 MHg
#define	CFG1101_0E		    0xA8		// FREQ1 (x)
#define	CFG1101_0F		    0x5E		// FREQ0 (x)
                                                        

                                                        // ** MDMCFG4 (x)* CHANBW81.250000 kHg
                                                        // ** MDMCFG3 (x)* DRATE38.383484 kbaud
                                                        // ** MDMCFG2 (x)* GFSK, MANCHESTER, 30/32 SYNC DETECT
                                                        // ** MDMCFG1 (x)* 12 bytes PREAMBLE

#define	PLT1101_0E		    0xB1		// FREQ1 (x)  433.999969 MHg
#define	PLT1101_0F		    0x3B		// FREQ0 (x)



                                                        // MDMCFG4 (x)* CHANBW=81.250000 kHg
#define	CFG1101_10		    (3<<CHANBW_E)|(1<<CHANBW_M)|(0x0A<<DRATE_E) // 0xDA


#define	CFG1101_11		    (0x83<<DRATE_M)	// MDMCFG3 (x)* DRATE=38.383484 kbaud
                                                        // MDMCFG2 (x)* GFSK, MANCHESTER, 30/32 SYNC DETECT
#define	CFG1101_12		    (0<<DEM_DCFILT_OFF)|(GFSK<<MOD_FORMAT)|(1<<MANCHESTER_EN)|(3<<SYNC_MODE) // 0x1B
                                                        // MDMCFG1 (x)* 12 bytes PREAMBLE
#define	CFG1101_13		    (0<<FEC_EN)|(5<<NUM_PREAMBLE)|(1<<CHANSPC_E) // 0x51

#define	CFG1101_14		    0xF8		// MDMCFG0 (x)* CHANSPC=99.975586 kHg
#define	CFG1101_15		    0x32		// DEVIATN (x)* 15.869141 kHg

                                                        // MCSM2 (x)* until end of packet
#define	CFG1101_16		    (0<<RX_TIME_RSSI)|(0<<RX_TIME_QUAL)|(7<<RX_TIME) // 0x07
                                                        // MCSM1 (x)* Always clear channel, RXOFF-RX, TXOFF-RX 
                                                        // TXOFF, RXOFF - RX
#define	CFG1101_17		    (0<<CCA_MODE)|(0<<RXOFF_MODE)|(0<<TXOFF_MODE) // 0x0F

                                                        // MCSM0 (x)* AUTOCAL IDLE-RX IDLE_TX, XOCS OFF
#define	CFG1101_18		    (1<<FS_AUTOCAL)|(2<<PO_TIMEOUT)|(0<<PIN_CTRL_EN)|(0<<XOSC_FORCE_ON) // 0x18

#define	CFG1101_19		    0x16		// FOCCFG (x)
#define	CFG1101_1A		    0x6D		// BSCFG (x)	+/-3.125 data rate offset
#define	CFG1101_1B		    0x43		// AGCCTRL2 (x)
#define	CFG1101_1C		    0x40		// AGCCTRL1 (x)
#define	CFG1101_1D		    0x91		// AGCCTRL0 (x)
#define	CFG1101_1E		    0x87		// WOREVT1 (x)
#define	CFG1101_1F		    0x6B		// WOREVT0 (x)
#define	CFG1101_20		    0xE8		// WORCTRL (x)
#define	CFG1101_21		    0x56		// FREND1 (x)
#define	CFG1101_22		    0x10		// FREND0 (x)
#define	CFG1101_23		    0xE9		// FSCAL3 (x)
#define	CFG1101_24		    0x2A		// FSCAL2 (x)
#define	CFG1101_25		    0x00		// FSCAL1 (x)
#define	CFG1101_26		    0x1F		// FSCAL0 (x)
#define	CFG1101_27		    0x41		// RCCTRL1
#define	CFG1101_28		    0x00		// RCCTRL0

#define	CFG1101_29		    0x59		// FSTEST (x)
#define	CFG1101_2A		    0x7F		// PTEST
#define	CFG1101_2B		    0x3F		// AGCTST
#define	CFG1101_2C		    0x81		// TEST2 (x)
#define	CFG1101_2D		    0x35		// TEST1 (x)
#define	CFG1101_2E		    0x09		// TEST0 (x)







// ** ������� �������� ��������� RCHPP

static const uint8_t CONFIG[] = {
        
CFG1101_00,CFG1101_01,CFG1101_02,CFG1101_03,CFG1101_04,CFG1101_05,CFG1101_06,CFG1101_07,CFG1101_08,CFG1101_09,CFG1101_0A,CFG1101_0B,CFG1101_0C,CFG1101_0D,CFG1101_0E,CFG1101_0F,
CFG1101_10,CFG1101_11,CFG1101_12,CFG1101_13,CFG1101_14,CFG1101_15,CFG1101_16,CFG1101_17,CFG1101_18,CFG1101_19,CFG1101_1A,CFG1101_1B,CFG1101_1C,CFG1101_1D,CFG1101_1E,CFG1101_1F,
CFG1101_20,CFG1101_21,CFG1101_22,CFG1101_23,CFG1101_24,CFG1101_25,CFG1101_26,CFG1101_27,CFG1101_28
};



// ** ��������� ����� gdo0 � gdo2
// ** �����, ��� ��������, ������� ����������
// ** ���������� �� �������� ������� �� gdo0 � gdo2

static void cc_configGdoPins()
{  

	GPIO_Init(GDO0_GPIO, GDO0_PIN, GPIO_Mode_In_PU_IT);
	GPIO_Init(GDO2_GPIO, GDO2_PIN, GPIO_Mode_In_PU_No_IT);	
	
    EXTI_SetPinSensitivity(EXTI_Pin_3, EXTI_Trigger_Falling);
	EXTI->SR1 = EXTI_GDO0_PIN;
}




static void cc_ready()
{
	GPIO_ResetBits(SPI_MOSI_GPIO, SPI_MOSI_PIN);
	GPIO_ResetBits(SPI_SCK_GPIO,  SPI_SCK_PIN);
	GPIO_ResetBits(SPI_CS_GPIO,   SPI_CS_PIN);
	
	WAIT_UNTIL_MISO_BECOMES_LOW;
}

static u8 cc_put(u8 byte)
{
		RCHPP_SPI->DR = byte;
		while(SPI_SR_RXNE != (RCHPP_SPI->SR & SPI_SR_RXNE));
		byte = RCHPP_SPI->DR;
    return byte;
}

// ****************************************************************
// ** ������ ����� �� ���������� �� ������
static u8 cc_read(u8 byte)
{
	
		cc_ready();
		cc_put(READ_SINGLE | byte );
		byte = cc_put(0);
		
		CS_HIGH;
		
	
	
    return byte;
}

static void cc_write(u8 addr, u8 byte)
{
	
		cc_ready();
		cc_put(addr);
		byte = cc_put(byte);
		
		CS_HIGH;
	
}

static u8 cc_strobe(u8 strobe)
{
	
		cc_ready();
		cc_put(READ_SINGLE | strobe );
		strobe = cc_put(READ_SINGLE | SNOP );
		CS_HIGH;

	
    return strobe;
}

// ****************************************************************
static void cc_writeBrust_FIFO(u8* ptr, const u8 len)
{
	
		cc_ready();
		cc_put(WRITE_BURST | TXFIFO);
		
		for(u8 i = 0 ; i < len ; i++)
		{
			cc_put(ptr[i]);
		}
		
		CS_HIGH;
	
}


static void cc_readBrust_FIFO(u8* ptr, const u8 len)
{
	
	cc_ready();
	cc_put(READ_BURST | TXFIFO);
	
	for(u8 i = 0 ; i < len ; i++)
	{
		ptr[i] = cc_put(0);
	}
	
	CS_HIGH;   
	
}
// ****************************************************************



static void cc_init()
{
	
	cc_ready();
	cc_put(WRITE_BURST);
	
	for(u8 i = 0; i < sizeof(CONFIG); cc_put(CONFIG[i++]));    
	
	CS_HIGH;
	
}

static void cc_reset()
{
	
	GPIO_SetBits(SPI_CS_GPIO, 	  SPI_CS_PIN);
	GPIO_ResetBits(SPI_MOSI_GPIO, SPI_MOSI_PIN);
  
	CS_LOW;
	CS_HIGH;
	
	CS_LOW;
	delays.us10(5);
	CS_HIGH;
	
	cc_strobe(SRES);
	cc_write(0x00, CONFIG[0]);
	cc_write(0x01, CONFIG[1]);
	cc_write(0x02, CONFIG[2]);
	EXTI->SR1 = EXTI_GDO0_PIN;
	
}










static void cc_setIDLE_SLEEPstate()
{
	cc.setIDLEstate();
	delays.ms(1); 
	cc_strobe(SPWD);
}


static void cc_setRXstate()
{
	while (STATE_IDLE != (cc_strobe(SFRX)  & STATE_MASK));
	while (STATE_IDLE != (cc_strobe(SFTX)  & STATE_MASK));
	while (STATE_RX   != (cc_strobe(SRX)   & STATE_MASK));
}

static void cc_setTXstate()
{
	while (STATE_IDLE != (cc_strobe(SFRX)  & STATE_MASK));
	while (STATE_IDLE != (cc_strobe(SFTX)  & STATE_MASK));
	while (STATE_TX   != (cc_strobe(STX)   & STATE_MASK));
}

static void cc_setIDLEstate()
{	
	while ( STATE_IDLE != (cc_strobe(SIDLE) & STATUS_STATE_BM));			
}



//// ��������� ������ ������ 
//static void cc_setPultSettings(const bool tmp)
//{  	
//  	if (tmp)
//	{
//		cc_write(0x0E, 0xB1);
//		cc_write(0x0F, 0x3B);
//	}
//	else 
//	{
//		cc_write(0x0E, CFG1101_0E);
//		cc_write(0x0F, CFG1101_0F); 	
//	}
//}
//
//// ��������� ������ ������ 
static void cc_setChannelNum(const u8 num)
{  	
  	while ( STATE_IDLE != (cc_strobe(SIDLE) & STATUS_STATE_BM));
    cc_write(0x0A, num);
}



// ��������� ��������
static void cc_setPower(const u8 pwr)
{  	
  	while ( STATE_IDLE != (cc_strobe(SIDLE) & STATUS_STATE_BM));
    cc_write(PATABLE, pwr);
}
/*!
    \brief ������� ��1101

    �������� ��� ������� ������� ��� ������ 
    � ��1101 
*/
const struct Cc_driver cc = 
{
	.read = cc_read,
	.write = cc_write,
	.strobe = cc_strobe,

	.writeBrust_FIFO = cc_writeBrust_FIFO,
	.readBrust_FIFO = cc_readBrust_FIFO,

	.reset = cc_reset,
	.init = cc_init,
	.configGdoPins = cc_configGdoPins,

	.setRXstate = cc_setRXstate,
	.setTXstate = cc_setTXstate,
	.setIDLEstate = cc_setIDLEstate,
	.setIDLE_SLEEPstate = cc_setIDLE_SLEEPstate,

	.setPower = cc_setPower,
	.setChannelNum = cc_setChannelNum,
};

