

#include "utils.h"
#include "MPU6050.h" 
#include "RCHPP.h"
#include "calcCRC.h"
#include "delay.h"

extern u8 Y[34];
extern u8 Z[34];

extern u8* const pZfunc;
extern int16_t x_offset, y_offset, z_offset;
extern s16 x, y, z;
extern u16 tmpVCC;
u16 tmpCRC;
extern const u8 NUMBER;







PT_THREAD (thread_radioSend(pt_t *pt))
{
	
  	PT_BEGIN(pt);

		Z[0] = 12;
		Z[1] = NUMBER;
		Z[2] = 0xFF;
		
		Z[3]  = BYTE_0(x);
		Z[4]  = BYTE_1(x);		
		Z[5]  = BYTE_0(y);
		Z[6]  = BYTE_1(y);		
		Z[7]  = BYTE_0(z);
		Z[8]  = BYTE_1(z);
		Z[9]  = BYTE_1(tmpVCC);
		Z[10] = BYTE_0(tmpVCC);
		
		tmpCRC = CRC16(pZfunc,Z[0]-2);
		
		Z[11] = BYTE_1(tmpCRC);
		Z[12] = BYTE_0(tmpCRC);
		
		
		
		/*
		// ���� ������ �����
		GDO0_ISR_DIS;	
		cc.setIDLEstate();
		cc.setRXstate();	
		tim.reset(RADIO_TIMER);
		do {		  
			if (IS_PREAMBLE_DETECTED) 
			{
				tim.reset(RADIO_TIMER);
			}
			PT_YIELD(pt);		  
		} while(tim.get(RADIO_TIMER) <= 30);
		cc.setIDLEstate();
		cc.setSLEEPstate();
		GDO0_ISR_EN;
		*/
		
		
		
		cc.setIDLEstate();
		cc.setTXstate();
		delays.ms(5 + NUMBER*5);
		cc.writeBrust_FIFO(Z, 32);	
		
		while(IS_GDO0_INACTIVE);
		while(IS_GDO0_ACTIVE);
		

		cc.setIDLEstate();
		cc.setIDLE_SLEEPstate();
		
		
	PT_END(pt);
}





