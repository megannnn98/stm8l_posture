/* Includes ------------------------------------------------------------------*/
#include "utils.h"
#include "MPU6050.h" 
#include "delay.h"
#include "eeCoding.h"
/* Private functions ---------------------------------------------------------*/

extern s8 x, y, z;
extern s8 x_offset, y_offset, z_offset;


pt_t mpu_pt;
pt_t btn_pt;
pt_t motor_pt;

PT_THREAD (thread_mpu(pt_t *pt));
PT_THREAD (thread_btn(pt_t *pt));
PT_THREAD (thread_motor(pt_t *pt));
void set_unused_pins_output();


void main( void )
{
  CLK_16MHz;    
	

	tim.init();		

  GPIO_Init(BTN_GPIO,         BTN_PIN,          GPIO_Mode_In_PU_No_IT);
  GPIO_Init(BLUE_GPIO,        BLUE_PIN,         GPIO_Mode_Out_PP_Low_Fast);  
  GPIO_Init(MOTOR_GPIO,       MOTOR_PIN,        GPIO_Mode_Out_PP_Low_Fast);
  GPIO_Init(MPU6050_PWR_GPIO, MPU6050_PWR_PIN,  GPIO_Mode_Out_PP_High_Slow);
  GPIO_Init(MPU6050_ISR_GPIO, MPU6050_ISR_PIN,  GPIO_Mode_In_PU_No_IT);  
  GPIO_Init(GREEN_GPIO,       GREEN_PIN,        GPIO_Mode_Out_PP_Low_Fast);
  set_unused_pins_output();

  
	enableInterrupts();	
  
  PT_INIT(&mpu_pt);
  PT_INIT(&btn_pt);
  PT_INIT(&motor_pt);

  
  MEM_readDataTwoCopyes(I_EE_ACCEL_X, &x_offset, 0);
  MEM_readDataTwoCopyes(I_EE_ACCEL_Y, &y_offset, 0);
  MEM_readDataTwoCopyes(I_EE_ACCEL_Z, &z_offset, 0);

	msg.init();
  

  msg.set(MPU_THREAD_RESET_MSG, 0, NULL);
	while(1)
	{
      thread_mpu(&mpu_pt);
      thread_btn(&btn_pt);      
      thread_motor(&motor_pt);   
      if (msg.getVal(MPU_THREAD_RESET_MSG))
      {
          PT_INIT(&mpu_pt);
      }
	}
}












