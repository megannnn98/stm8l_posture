/* Includes ------------------------------------------------------------------*/
#include "utils.h"
#include "MPU6050.h" 
#include "delay.h"
#include "eeCoding.h"
/* Private functions ---------------------------------------------------------*/

extern s8 x, y, z;
extern s8 x_offset, y_offset, z_offset;
extern enum 
{
    NOT_READY = 0,
    READY
} motor_ready;


PT_THREAD (thread_motor(pt_t *pt))
{
    PT_BEGIN(pt);
    
    while(1)
    {
        if (READY == motor_ready)
        {
            if (ON_CONDITION)
            {
                msg.set(MOTOR_MSG, NULL, 0);
            }
            
            if (msg.getVal(MOTOR_MSG))
            {
                if (OFF_CONDITION)
                {
                    break;
                }
                else
                {
                    BLUE_GPIO->ODR |= BLUE_PIN;
                    MOTOR_GPIO->ODR |= MOTOR_PIN;
                    
                    PT_DELAY_MS(MOTOR_TIMER, 200);
                    
                    BLUE_GPIO->ODR &= ~BLUE_PIN;
                    MOTOR_GPIO->ODR &= ~MOTOR_PIN;
                    
                    PT_DELAY_MS(MOTOR_TIMER, 200);
                    
                    BLUE_GPIO->ODR |= BLUE_PIN;
                    MOTOR_GPIO->ODR |= MOTOR_PIN;
                    
                    PT_DELAY_MS(MOTOR_TIMER, 200);
                      
                    BLUE_GPIO->ODR &= ~BLUE_PIN;
                    MOTOR_GPIO->ODR &= ~MOTOR_PIN;
                    
                    PT_DELAY_MS(MOTOR_TIMER, 1000);
                      
                }
            }
        }
        
        BLUE_GPIO->ODR &= ~BLUE_PIN;
        MOTOR_GPIO->ODR &= ~MOTOR_PIN;
                
        PT_YIELD(pt);
    }
    PT_END(pt);
}













