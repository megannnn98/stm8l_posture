


#pragma once

#include "stm8l15x.h"

#define I_EE_ACCEL_X     (u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS
#define I_EE_ACCEL_Y     (u32)I_EE_ACCEL_X + 6
#define I_EE_ACCEL_Z     (u32)I_EE_ACCEL_Y + 6

#define II_EE_ACCEL_X    (u32)FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS   + FLASH_BLOCK_SIZE
#define II_EE_ACCEL_Y    (u32)II_EE_ACCEL_X + 6
#define II_EE_ACCEL_Z    (u32)II_EE_ACCEL_Y + 6


#define EEPROM_LOCK     do {  FLASH->IAPSR &= (uint8_t)FLASH_MemType_Data;    \
                              FLASH->IAPSR &= (uint8_t)FLASH_MemType_Program; \
                           }while(0)
                             
                      
#define EEPROM_UNLOCK   do {  FLASH->PUKR = FLASH_RASS_KEY1; \
                              FLASH->PUKR = FLASH_RASS_KEY2; \
                              FLASH->DUKR = FLASH_RASS_KEY2; \
                              FLASH->DUKR = FLASH_RASS_KEY1; \
                           }while(0)

							 
#define STATUS_WAS_ERROR	2
#define STATUS_OK			1
#define STATUS_ERROR		0

#define MEM_readByte 		FLASH_ReadByte
#define taskENTER_CRITICAL();				
#define taskEXIT_CRITICAL();
#define MEM_BLOCK_SIZE		FLASH_BLOCK_SIZE
							 
							 

void MEM_readPkt(u16 startAddr, u8* ptr, const u16 len);
void MEM_writePkt(u16 startAddr, u8* ptr, const u16 len);

void MEM_write32withCRC(uint16_t adress16, void* dataPtr);
uint8_t MEM_read32withCRC(uint16_t adress16, void* dataPtr);
uint8_t MEM_writeDataTwoCopyes(uint16_t adress, void* dataPtr);
uint8_t MEM_readDataTwoCopyes(uint16_t adress, void* dataPtr, uint32_t defaultVal);